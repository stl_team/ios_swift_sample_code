
import Foundation
import UIKit

// ENUM for alert type
enum AlertType : Int {
    // Green alert view with check mark image.
    case success = 0
    // Red alert view with error image
    case error = 1
    // Orange alert view with warning image
    case warning = 2
    // Light green alert with info image.
    case info = 3
    // Custom alert, default - info image and light blue background color
    case custom = 4
}

// ENUM for Button Tag
enum selectedButton : Int {
    
    case buttonTotalVehicle = 101
    case buttonRunning
    case buttonIdle
    case buttonNoComm
}

struct DrawerArray {
    static let array:NSArray = ["My Spot","Requests","Change Password", "Log out"]
    static let imageArray: NSArray = ["my_spot_drawer","request_drawer","change_pass_drawer","log_out_drawer"]
}

class CommonViewController: UIViewController {
    
    // Global function to add any particular sides border for View
    func addBordersToEdge(edge: UIRectEdge,color: UIColor, borderWidth: CGFloat,theView: UIView ) {
        
        // will only add top baorder for View
        if edge == UIRectEdge.top{
            let border: UIView = UIView()
            border.backgroundColor = color
            border.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
            border.frame = CGRect(x: 0, y: 0, width: theView.frame.size.width, height: borderWidth)
            theView.addSubview(border)
        }
        
            // will only add left baorder for View
        else if edge == UIRectEdge.left{
            let border: UIView = UIView()
            border.backgroundColor = color
            border.autoresizingMask = [.flexibleHeight, .flexibleRightMargin]
            border.frame = CGRect(x: 0, y: 0, width: borderWidth, height: theView.frame.size.height)
            theView.addSubview(border)
        }
            
            // will only add bottom baorder for View
        else if edge == UIRectEdge.bottom{
            let border: UIView = UIView()
            border.backgroundColor = color
            border.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
            
            border.frame = CGRect(x: 0, y: theView.frame.size.height - borderWidth, width: theView.frame.size.width, height: borderWidth)
            theView.addSubview(border)
        }
            
            // will only add right baorder for View
        else if edge == UIRectEdge.right{
            let border: UIView = UIView()
            border.backgroundColor = color
            border.autoresizingMask = [.flexibleHeight, .flexibleLeftMargin]
            border.frame = CGRect(x: theView.frame.size.width-borderWidth, y: 0, width: borderWidth, height: theView.frame.size.height)
            theView.addSubview(border)
        }
    }

    //To check any fields empty in UIViewController
    func checkEmptyTextFieldInView(theView: UIView,viewController: UIViewController) -> (Bool) {
        for case let textField as UITextField in theView.subviews {
            if textField.text == "" {
                // show error
                self.showAlert(message: EMPTY_FIELD_MESSAGE, viewController: viewController)
                return false
            }
        }
        return true
    }

    // Customize UITabbar appearance
    func customizeTabbarAppearance () {
        let colorNormal : UIColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.5)
        let colorSelected : UIColor = UIColor.white
        let titleFontAll : UIFont = UIFont(name: FONT_REGULAR, size: 13.0)!
        
        // Change Font style and foregorund color for normal state
        let attributesNormal = [
            NSAttributedStringKey.foregroundColor : colorNormal,
            NSAttributedStringKey.font : titleFontAll
        ]
        
        // Change Font Style and foreground color for selected state
        let attributesSelected = [
            NSAttributedStringKey.foregroundColor : colorSelected,
            NSAttributedStringKey.font : titleFontAll
        ]
        
        UITabBarItem.appearance().setTitleTextAttributes(attributesNormal, for: UIControlState.normal)
        UITabBarItem.appearance().setTitleTextAttributes(attributesSelected, for: UIControlState.selected)
        
        // Change tint color for normal state and selcted state
        UITabBar.appearance().unselectedItemTintColor = colorNormal
        UITabBar.appearance().tintColor = colorSelected
        
        // Change background image for UITabbar
        self.tabBar.backgroundImage = UIImage(named: "navigation_bar")
        self.tabBar.autoresizesSubviews = false
        self.tabBar.clipsToBounds = true
    }
    
    // Global right navigation button setup in BaseViewController class
    func rightBarButton(strTitle: String)
    {
        let buttonRight = UIButton (frame:CGRect(x: 0, y: 0, width: 60, height: 44))
        
        buttonRight.tintColor = UIColor.white
        buttonRight.setTitle(strTitle, for:.normal)
        buttonRight.titleLabel?.textAlignment = .right
        buttonRight.addTarget(self, action: #selector(rightBarButtonClick(sender:)), for: .touchUpInside)
        buttonRight.titleLabel?.font =  UIFont(name: FONT_BOLD, size: 17)
        buttonRight.contentHorizontalAlignment = .right;
        
        
        let rightBarButton = UIBarButtonItem(customView: buttonRight)
        self.navigationItem.rightBarButtonItem = rightBarButton
       
    }
}

// Extension for UITextfield
extension CommonViewController: UITextField {
    
    // To customize Placeholder color for Textfield
    func changePlaceholderColor(color: UIColor) {
        
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                        attributes: [NSAttributedStringKey.foregroundColor:color])
    }
    
    // To customize Border color for Textfield
    func changeBorderColor(color: UIColor) {
        self.addBordersToEdge(edge: UIRectEdge.bottom, color: color, borderWidth: 1.0, theView: self)
        
    }
}
